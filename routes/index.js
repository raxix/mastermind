/*jshint node: true */
/*jshint -W009 */
exports.index = function (req, res) {
    req.session.puzzle = req.session.puzzle || req.app.get('puzzle');
    res.render('index', {
        title: 'Mastermind'
    });
};

exports.play = function (req, res) {
    var newGame = function () {

        // Przygotowanie potrzebnych danych
        var dataGame = {
            size: Math.floor(req.query.size),
            dim: Math.floor(req.query.dim),
            max: Math.floor(req.query.max),
            move: 1
        };

        // Zapisywanie danych z gry w sesji
        req.session.puzzle = dataGame;

        // Generowanie poprawnych odpowiedzi
        var i, data = [],
            puzzle = req.session.puzzle;
        for (i = 0; i < puzzle.size; i += 1) {
            data.push(Math.round(Math.random() * puzzle.dim));
        }
        req.session.puzzle.data = data;
        //wypisanie wygenerowanych danych przez serwer w konsoli
        console.log("Ciąg elementów wylosowanych przez serwer: " + data);

        // Zwrot wprowadzonych danych
        return {
            "results": "Wprowadzone przez Ciebie nastepujące dane:" + "<br/>" +
                " Liczba elementów w rzędzie: " + dataGame.size + "<br/>" +
                " Liczba używanych kolorków: " + dataGame.dim  + "<br/>" +
                " Liczba prób: " + dataGame.max,
            "game": {
                size: puzzle.size,
                move: 1
            }
        };
    };

    res.json(newGame());
};

exports.mark = function (req, res) {
    var markAnswer = function () {
        var game = req.session.puzzle;
        var correctAnswers = game.data.slice();
        var userAnswers = req.query.results;

        // Szukanie poprawnych odpowiedzi
        var markCorrectAnswers = new Array();
        userAnswers.forEach(function(value, key) {
            if(correctAnswers[key] == value) {
                markCorrectAnswers.push(1);
                correctAnswers[key] = "checked";
                userAnswers[key] = "checked";
            }
        });

        userAnswers.forEach(function(valueUser, keyUser){
            if(valueUser != "checked") {
                correctAnswers.forEach(function(valueCorrect, keyCorrect){
                    if(valueUser == valueCorrect) {
                        markCorrectAnswers.push(0);
                        userAnswers[keyUser]       = "checked";
                        correctAnswers[keyCorrect] = "checked";
                        return;
                    }
                });
            }
        });

        markCorrectAnswers = markCorrectAnswers.sort();
        game.move += 1;

        return {
            "move": game.move,
            "maxMove": game.max,
            "size": game.size,
            "answers": markCorrectAnswers,
            "correctAnswers": game.data
        };
    };
    res.json(markAnswer());
};